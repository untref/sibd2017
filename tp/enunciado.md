# Trabajo práctico 2017
# Sistemas de información y base de datos - UNTREF

## enunciado

Escribir las instrucciones SQL necesarias para generar dos cuadros con la información de la EAH 2016 de CABA.
Uno de los cuadros deberá ser alguno de los publicados por la página web de la DGEyC con información de la EAH 2016.
El otro cuadro deberá ser uno similar al anterior pero que contenga más información. 

## desarrollo

Deberá entregarse vía mail en un archivo .zip:
   1. un texto (formato docx) donde:
      1. se explique lo hecho paso a paso (incluyendo las instrucciones SQL utilizadas)
      2. se muestren los resultados en sendos cuadros (con el mejor formato posible).
   2. un par de backups de la base de datos en sus estados inicial y final
   3. los archivos de insumos o intermedios que se hayan generado para el trabajo (.txt .sql etc)
   4. los archivos con los cuadros (.xslx o el formato que se haya usado para generar los cuadros)
   
## limitaciones

   1. Las instrucciones SQL pueden simplemente generar los datos, no es necesario que lo hagan en forma de tabla
   2. No es necesario que las instrucciones SQL calcules los totales de filas o columnas (eso se puede hacer después al generar la tabla).
   
## Fecha de entrega

El día del parcial.

## vínculos
  1. Base a usuario http://www.estadisticaciudad.gob.ar/eyc/?p=69945
  2. Cuadros de resultados (tabulados básicos) http://www.estadisticaciudad.gob.ar/eyc/?cat=84