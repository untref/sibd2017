# untref/sibd2017

<img style="float: right;" align="right" src="imagenes/logo_untref_celeste.png">

* **Universidad Nacional de Tres de Febrero**
* **Maestría en Generación y Análisis de Información Estadística**

# Sistemas de Información y Base de Datos - año 2017

## Clases

   * *La mayoría de las clases se van a dictar los miércoles. Habrá alguna clase los días lunes.*

<!--
 17/8  | **labo ✓**  | **labo ✓**  | [clase 2](pdfs/SIyBD-clase%202.pdf) | [transacciones](clases/clase2.md) | [clase2_db](dbs/clase2_db.backup)
-->
   
  fecha| ubicación      | pdf  | laboratorio | db
-------|----------------|------|------|----
  9/8  | **aula ✗**     | [clase 1](pdfs/SIyBD-clase%201.pdf) | [contando viviendas](clases/clase1.md) | [clase1_db](dbs/clase1_db.backup)
 17/8  | **labo ✓**     | [clase 2](pdfs/SIyBD-clase%202.pdf) | [transacciones](clases/clase2.md) |
 24/8  | **labo ✓**     | [clase 3](pdfs/SIyBD-clase%203.pdf) | [variables calculadas](clases/clase3.md) |  [clase3_db](dbs/clase3_db.backup)
 30/8  | **labo ✓**     | [clase 4](pdfs/SIyBD-clase%204.pdf) | [casos de prueba](clases/clase4.md)   |  |
  6/9  | **labo ✓**     | [clase 5](pdfs/SIyBD-clase%205.pdf) | [nietos](clases/clase5.md)  |  |
lu 11/9| **labo ✓**     |             |   |  |
 13/9  | **labo ✓**     |             |   |  |
 20/9  |**NO HAY CLASE**|             |   |  |
 27/9  |                |             |   |   |
 4/10  |**NO HAY CLASE**|             |   |   |
lu 9/10| *a confirmar*  | **EXAMEN**  |   |   |

## Recursos

* [Software necesario para la materia](software.md#software-a-instalar-untref-siybd-2017)
* [Sitios](sitios.md#sitios-de-referencia-untref-siybd-2016) de referencia, con [información](sitios.md#información) o [herramientas](sitios.md#herramientas)

