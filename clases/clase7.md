Ejemplos vistos en clase

```sql
SELECT comuna, t40, 100.0*count(*)/
    (SELECT count(*)
	from miembros z
	where estado=1
          and x.comuna=z.comuna
    )
  from miembros x
  where estado=1
  group by comuna, t40
  order by comuna, t40;

CREATE OR REPLACE VIEW comunas_cond AS
  SELECT comuna, 
         sum(case when estado=1 then fexp else 0 end) as ocupados,
         sum(case when estado=2 then fexp else 0 end) as desocupados,
         sum(case when estado=3 then fexp else 0 end) as inactivos
    FROM miembros
    GROUP BY comuna;

CREATE OR REPLACE VIEW comunas_cond2 AS
  SELECT *, ocupados+desocupados as activos
    FROM comunas_cond;

SELECT comuna, 
       100.0*desocupados/activos as tasa_desocup,
       100.0*activos/(activos+inactivos) as tasa_actividad
  FROM comunas_cond2
  ORDER BY comuna;
  
CREATE OR REPLACE VIEW comunas_cond3 AS
  SELECT comuna, 
         sum(case when estado=1 then fexp else 0 end) as ocupados,
         sum(case when estado=2 then fexp else 0 end) as desocupados,
         sum(case when estado in (1,2) then fexp else 0 end) as activos,
         sum(case when estado=3 then fexp else 0 end) as inactivos,
         sum(case when estado in (1,2,3) then fexp else 0 end) as total
    FROM miembros
    GROUP BY comuna;

SELECT *, 
       100.0*desocupados/activos as tasa_desocup,
       100.0*activos/total as tasa_actividad
  FROM comunas_cond3
  ORDER BY comuna;  

SELECT *, 
       100.0*desocupados/activos as tasa_desocup,
       100.0*activos/total as tasa_actividad
  FROM comunas_cond3
  UNION 
  SELECT 99, sum(ocupados), sum(desocupados), sum(activos),
        sum(inactivos), sum(total),
       100.0*sum(desocupados)/sum(activos) as tasa_desocup,
       100.0*sum(activos)/sum(total) as tasa_actividad
    FROM comunas_cond3
  ORDER BY comuna;
  
```