# software a instalar (untref-SIyBD-2017)

Esta es la lista de software a instalar para las prácticas de la materia, 
en cada link debe elegirse el instalador en base al sistema operativo instalado en cada máquina. 

**Usuarios de windows** deben saber si su sistema operativo está instalado en 64bits o 32bits 
(ver [¿Cómo sé en cuántos bits trabaja mi windows?](detectar-windows.md#c%C3%B3mo-s%C3%A9-en-cu%C3%A1ntos-bits-trabaja-mi-windows))

Postgresql 9.5+
   * [http://www.enterprisedb.com/products-services-training/pgdownload#windows](http://www.enterprisedb.com/products-services-training/pgdownload#windows)  o
   * [https://www.postgresql.org/download/](https://www.postgresql.org/download/)

Notepad++ 7.4+
   * [https://notepad-plus-plus.org/download/v7.4.2.html](https://notepad-plus-plus.org/download/v7.4.2.html)

GIT
   * [https://git-scm.com/](https://git-scm.com/)

TortoiseGIT
   * [https://tortoisegit.org/download/](https://tortoisegit.org/download/)

7-zip
   * [http://www.7-zip.org/](http://www.7-zip.org/)

Chrome o Firefox
   * [https://www.mozilla.org/es-ES/firefox/new/](https://www.mozilla.org/es-ES/firefox/new/) o
   * [https://www.google.com/chrome/browser/desktop/](https://www.google.com/chrome/browser/desktop/)

## Quizás usemos
   
NodeJS 8.3+
   * [https://nodejs.org/en/](https://nodejs.org/en/)

R
   * [https://cran.r-project.org/mirrors.html](https://cran.r-project.org/mirrors.html) y 
   * [https://www.rstudio.com/](https://www.rstudio.com/)
